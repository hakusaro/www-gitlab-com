---
layout: markdown_page
title: "Discussion Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Discussion Team
{: #discussion}

The Discussion Team is focused on the collaboration functionality of GitLab.

This team maps to [Plan and Create](/handbook/product/categories/#dev).
